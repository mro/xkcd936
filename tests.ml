open Alcotest

let tc_back () =
  let bs = Bigstring.of_string "ab\ncd\n" in
  check int __LOC__ 0 (Lines.back bs 0);
  check int __LOC__ 0 (Lines.back bs 1);
  check int __LOC__ 0 (Lines.back bs 2);
  check int __LOC__ 3 (Lines.back bs 3);
  check int __LOC__ 3 (Lines.back bs 4);
  check int __LOC__ 3 (Lines.back bs 5)

let tc_forw () =
  let bs = Bigstring.of_string "ab\ncd\n" in
  let l = Bigarray.Array1.dim bs in
  check int __LOC__ 6 (l);
  check int __LOC__ 2 (Lines.forw bs l 0);
  check int __LOC__ 2 (Lines.forw bs l 1);
  check int __LOC__ 2 (Lines.forw bs l 2);
  check int __LOC__ 5 (Lines.forw bs l 3);
  check int __LOC__ 5 (Lines.forw bs l 4);
  check int __LOC__ 5 (Lines.forw bs l 5)

let tc_line () =
  let bs = Bigstring.of_string "ab\ncd\n" in
  let l = Bigarray.Array1.dim bs in
  check int __LOC__ 6 l;
  check string __LOC__ "ab" (Lines.line bs l 0);
  check string __LOC__ "ab" (Lines.line bs l 1);
  check string __LOC__ "ab" (Lines.line bs l 2);
  check string __LOC__ "cd" (Lines.line bs l 3);
  check string __LOC__ "cd" (Lines.line bs l 4);
  check string __LOC__ "cd" (Lines.line bs l 5)

let () =
  run
    "Xkcd936" [
    __FILE__ , [
      "tc_back", `Quick, tc_back;
      "tc_forw", `Quick, tc_forw;
      "tc_line", `Quick, tc_line;
    ]
  ]
